from django import forms
import datetime
class Form_Daftar(forms.Form):

    error_messages = {
        'required': 'Please type',
    }

    nama_attrs = {
        'type': 'text',
        'placeholder' : 'Nama Lengkap',
        'class' : 'form-control',
    }

    tanggal_lahir_attrs = {
        'type': 'date',
        'placeholder' : 'Tanggal Lahir',
        'class' : 'form-control',
    }

    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
    }

    password_attrs = {
        'type': 'password',
        'placeholder': 'Password',
        'class' : 'form-control'
    }

    nama = forms.CharField(label='Nama Lengkap', required=True, widget=forms.TextInput(attrs = nama_attrs))
    tanggal_lahir = forms.DateField(label='Tanggal Lahir', required=True, widget=forms.DateInput(attrs = tanggal_lahir_attrs))
    email = forms.EmailField(label='Alamat Email', required=True, widget=forms.TextInput(attrs = email_attrs))
    password = forms.CharField(required=True, widget = forms.PasswordInput(attrs = password_attrs))

class Form_Donasi(forms.Form):
    error_messages = {
        'required': 'Please type',
    }

    nama_program_attrs = {
        'type': 'text',
        'placeholder' : 'Nama Program',
        'class' : 'form-control',
    }

    nama_attrs = {
        'type': 'text',
        'placeholder' : 'Nama Lengkap',
        'class' : 'form-control',
    }

    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
    }

    jumlah_attrs = {
        'type': 'number',
        'placeholder': 'Jumlah Donasi',
        'class' : 'form-control',
    }

    nama_program = forms.CharField(label='Nama Program', required=True, widget=forms.TextInput(attrs = nama_program_attrs))
    nama = forms.CharField(label='Nama Lengkap', required=True, widget=forms.TextInput(attrs = nama_attrs))
    email = forms.EmailField(label='Alamat Email', required=True, widget=forms.TextInput(attrs = email_attrs))
    jumlah = forms.IntegerField(required=True, widget = forms.NumberInput(attrs = jumlah_attrs))