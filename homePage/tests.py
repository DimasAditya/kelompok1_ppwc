from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class homePageTest(TestCase):
	def test_homePage_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_homePage_using_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'homePage.html')

	def test_homePage_using_home_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_homepage_header_display(self):
		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn('No one has ever', html_response)
	

