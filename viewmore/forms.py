from django import forms
from .models import Status
from django.forms import ModelForm

class Status_Message(forms.ModelForm):
	Kabar = forms.CharField(max_length= 300,label='',widget=forms.Textarea(attrs={ 'placeholder' : 'how is life?', 'rows' : 5}))
	class Meta:
		model = Status
		fields = ['Kabar']